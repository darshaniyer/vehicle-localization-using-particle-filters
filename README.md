## Localization of a kidnapped vehicle using particle filter

What is localization in robotics? A robot takes information about its current environment and compares that to information (eg., a known map) that it already knows about the real world to know where it is. Humans do something similar. Imagine you were suddenly kidnapped and blindfolded, and stuffed into a car that drove for hours - you would have no idea where in the world you were. Then, the blindfold was removed and you saw Eiffel tower; you immediately recognize you are in Paris. This is quite remarkable - before the blindfold was removed, you had zero undrestanding about your location; but after showing a tiny amount of data, a single image, you reduced that infinite uncertainty to a few km radius. This is the main intuition behind localization.

Imagine a car that is totally lost, which means you as a driver have no clue about your location. Now, assume you have a global map of the environment. The process of localization will help track down the ego car location with high accuracy of 3 to 10 cm. In a traditional approach, we use GPS to find the car with respect to the map; but GPS is not precise enough, i.e., has an accuracy of 1 to 3m, and sometimes as much as 10 to 50m. Thus, another accurate technique is needed.

It is a common practice to use onboard sensor data along with our global map to solve the localization problem. With onboard sensor, it is possible to measure distances to static obstacles like trees, poles, or walls. We measure these distances, and the bearing of the these static objects in the local coordinate system of our car. If these objects are also part of the global map, which has its own global coordinate system, to estimate the location of the car in the map, we have to match the sensor information with the map information. When done correctly, this leads to transformation between the local car coordinate system and the global coordinate system of the map. This transformation should be accurate as possible - within 10cm. If we are able to estimate this transformation, we have solved the localization issue.

The robot has to solve the global localization problem using sensor measurements, with which it has to determine a good posterior distribution as to where it is. The particle filters represent this localization problem using particles, a discrete guess as to where the robot might be comprise a single guess. A single guess in itself is not a filter, but a set of such guesses together comprise an approximate representation for the posterior distribution (probability of location given observations) of the robot. In the beginning, the particles are uniformly spread, but the particle filter makes them survive in proportion to how consistent the particles are with the sensor measurement. The essence of the particle filter is to have these particles guess as to where the robot might be moving, but also have them survive with the 'survival of the fittest' paradigm, so that the more consistent particles are more likely to survive. As a result, places of high probability will collect more particles, and therefore, be more representative of the robot's posterior belief, as the robot localizes itself. 


## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Kidnapped-Vehicle-Project).

## Code base

The project was implemented in Visual Studio in Windows 10.

1. In the CarND-Kidnapped-Vehicle-Project, open the kidnap_vehicle.sln file.
2. Rebuild Solution.
3. Open the simulator.
4. Click F5 in Visual Studio to debug. This attaches the code to the simulator.
5. Select "Dataset 1" and click "Start" on the simulator.

To compile the code in Linux or Mac OS or Docker environment, 
1. Go to CarND-Kidnapped-Vehicle-Project\src folder
1. Delete main.cpp 
2. Rename main_other_env.cpp as main.cpp.

## Detailed writeup

Detailed report can be found in [_Localization_writeup.md_](Localization_writeup.md).

## Solution video

![](./videos/ParticleFilter_demo.mp4)

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the blog post that helped me setup the Visual studio environment for this project - http://www.codza.com/blog/udacity-uws-in-visualstudio.

