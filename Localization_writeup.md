
## Localization of a kidnapped vehicle using particle filter

What is localization in robotics? A robot takes information about its current environment and compares that to information (eg., a known map) that it already knows about the real world to know where it is. Humans do something similar. Imagine you were suddenly kidnapped and blindfolded, and stuffed into a car that drove for hours - you would have no idea where in the world you were. Then, the blindfold was removed and you saw Eiffel tower; you immediately recognize you are in Paris. This is quite remarkable - before the blindfold was removed, you had zero undrestanding about your location; but after showing a tiny amount of data, a single image, you reduced that infinite uncertainty to a few km radius. This is the main intuition behind localization.

Imagine a car that is totally lost, which means you as a driver have no clue about your location. Now, assume you have a global map of the environment. In a traditional approach, we use GPS to find the car with respect to the map; but GPS is not precise enough, i.e., has an accuracy of 1 to 3m, and sometimes as much as 10 to 50m. We need to track down the ego car location with high accuracy of 3 to 10 cm. Thus, another accurate technique is needed.

It is a common practice to use onboard sensor data along with our global map to solve the localization problem. With onboard sensor, it is possible to measure distances to static objects like trees, poles, or walls. We measure these distances, and the bearing of the these static objects in the local coordinate system of our car. If these objects are also part of the global map, which has its own global coordinate system, to estimate the location of the car in the map, we have to match the sensor information with the map information. When done correctly, this leads to transformation between the local car coordinate system and the global coordinate system of the map. This transformation should be as accurate as possible - within 10cm. If we are able to estimate this transformation, we have solved the localization problem.

###  Markov localization 

Markov localization or Bayes filter for localization is a generalized filter for localization, and all other localization approaches are realizations of this approach. In this approach, we generally think of our vehicle location as a probability distribution - each time we move (motion step), our distribution becomes more diffused (wider); we pass our variables (map data, landmark observation data, and control data) into the filter (sensing step) to concentrate (narrow) this distribution, at each time step. Each state prior to applying our filter represents our *prior distribution*, and the narrowed distribution after applying the filter represents our *Bayes posterior distribution*.

[image1]: ./figures/Localization_posterior.jpg "Localization model"
![alt text][image1]
**Figure 1 Localization model**

As shown in Figure 1, we have a map $m$ with all the landmarks in a global coordinate system, $z$ observations from the onboard sensor in the local coordinate system, and information $u$ regarding how the car moves in between two time steps (distance between time steps $t$ and $t-1$, yaw/pitch/roll rates and velocity). We assume these variables are known. If the transformation between the local coordinate system of the car and the global coordinate system of the map is estimated, we can estimate the pose $x_{t}$ of the car in the global map. For 2D map, pose $x_{t}$ includes the position with $x$ and $y$ coordinates, and also the orientation $\theta$. Since $x_{t}$ can never be known with perfect accuracy, we want to form sufficiently accurate belief of the state $x_{t}$ in a probabilistic way. Localization is all about estimating the probability distribution of the state $x_{t}$ under the condition that all the previous observations $z$ from time 1 to $t$, all the controls $u$ from time 1 to $t$, and map are given. We assume the map is correct and that it does not change. If we would like to estimate the map also, then we would solve the simultaneous localization and mapping (SLAM) problem.

###  Bayes filter for localization 

We can apply Bayes rule to vehicle localization problem by passing variables through Bayes filter for each time step as our vehicle moves. The generalized form of Bayes filter for localization is given by P(a/b) = P(b/a).P(a)/P(b), where

* P(a/b) is P(location/observation), the normalized probability of a location given an observation called bayesian posterior
* P(b/a) is P(observation/location), the probability of an observation given a position (likelihood)
* P(a) is P(location), the prior probability of a location
* P(b) is P(observation), the total probability of an observation.

The likelihood P(observation/location) is determined by the observation model, and the prior P(location) is determined by the motion model.

Bayes filter is framework for recursive state estimation. We use previous state, $bel(x_{t-1})$ to estimate new state $bel(x_{t})$ using only current observations and controls (and not the whole history of data) under Markov assumption. The motion model describes the prediction step of the filter, and the observation model describes the update step. The state estimation using the Bayes filter is dependent upon the interaction between the prediction (motion model) and the update (observation model) steps. This is how we do probabilistic reasoning and recursive state estimation. 1D Markov localization, Kalman filters, and particle filters are realizations of Bayes filter.

The goal of this project is to implement the localization algorithm using particle filters to locate a kidnapped vehicle. The problem is an instance of sparse localization since we localize a car relative to a sparse set of landmarks. We will implement 2D particle filter using a map (landmarks in map coordinates) and some initial localization information (GPS coordinates). At each time step, we will also get observation (in vehicle coordinates) and control data (velocity and yaw rate). The success of the project is measured in terms of 1) number of particles to converge to target location, 2) time to converge to target location, 3) amount of noise in the GPS coordinates and landmark coordinates. The goal is to locate the kidnapped vehicle within 100 seconds using minimum number of particles with highest possible noise in the GPS coordinates and landmark coordinates.

###  Particle filter for localization 

Particle filters have continuous state space, can represent multimodal distributions, and approximate correct posterior distribution. They are easiest to program and are most flexible.

The robot has to solve the global localization problem using sensor measurements, with which it has to determine a good posterior distribution as to where it is. The particle filters represent this localization problem using particles, a discrete guess as where the robot might be - ($x$, $y$, $\theta$) comprise a single guess. A single guess in itself is not a filter, but a set of such guesses together comprise an approximate representation for the posterior distribution (probability of location given observations) of the robot. In the beginning, the particles are uniformly spread, but the particle filter makes them survive in proportion to how consistent the particles are with the sensor measurement of the robot. The essence of the particle filter is to have these particles guess as to where the robot might be moving, but also have them survive with the 'survival of the fittest' paradigm, so that the more consistent particles are more likely to survive. As a result, places of high probability will collect more particles, and therefore, be more representative of the robot's posterior belief, as the robot localizes itself. 

####  Theory of particle filters 

##### Measurement update: Bayes theorem

P(X/Z) $\alpha$ P(Z/X).P(X)

P(X/Z) is the posterior probability of location given the observation measurement Z, P(Z/X) is the likelihood of measurement Z given the state X, and P(X) is the prior probability of state itself. A set of particles together constitute the prior P(X). The likelihood P(Z/X) is represented with the importance weights. Particles together with their importance weights are a representation of the posterior distribution. But we want to get rid of importance weights. By resampling, we work the importance weights back into the set of particles so that the resulting particles representing the posterior P(X/Z) would represent the true posterior. After each resampling step, instead of particles being randomly located, we get particles that are more co-located than before. Initially, the orientations are different, but they too start converging after few iterations.

##### Motion (prediction) update: Law of total probability

P(X') = $\sum$ P(X'/X).P(X)

P(X) is prior probability of state (posterior probability from last iteration), P(X'/X) is the transition probability, $\sum$ represents convolution operation, and P(X') is the robot state after motion. You sample from the sum by taking a random particle and apply a motion model with a noise model to generate a random particle X'. As a result, you get a new particle set P(X'), that is correct distribution after the robot motion. 

###  Particle filter algorithm for localization 

Figure 2 represents the steps of the particle filter algorithm as well as its inputs.

[image2]: ./figures/ParticleFilter_overall_flow.jpg "Overall flow of particle filter algorithm for localization"
![alt text][image2]
**Figure 2 Overall flow of particle filter algorithm for localization**

Figure 3 depicts the pseudocode of the particle filter algorithm, which is an outline of steps we will need to take in order to implement a particle filter for localizing an autonomous vehicle. The pseudocode steps correspond to the steps in the algorithm flow chart depicted in Figure 2 - initialization, prediction, particle weight updates, and resampling.

[image3]: ./figures/ParticleFilter_pseudocode.jpg "Pseudocode of particle filter algorithm for localization"
![alt text][image3]
**Figure 3 Pseudocode of particle filter algorithm for localization**

At the initialization step (step 1), we estimate our position from GPS input. The subsequent steps in the process will refine this estimate to localize our vehicle. During the prediction step (step 3) we add the control input (yaw rate & velocity) for all particles. During the update step (steps 4 and 5), we update our particle weights using map landmark positions and feature measurements. During resampling (steps 7 to 10), we will resample M times (M is range of 0 to length of particle array) drawing a particle $i$ ($i$ is the particle index) proportional to its weight, using the resampling wheel algorithm. The new set of particles represent the Bayes filter posterior probability. We now have a refined estimate of the vehicles position based on the input evidence.


####  Initialization 

At the initialization step, we estimate our position from GPS input. 

We have to decide how many particles we want to use, which is decided empirically. As the number of particles approach infinity, the particles will exactly represent Bayesian posterior distribution; but having too many particles will slow down our filter, and prevent it from localizating a self driving car in real time. On the other hand, if we have too few particles, we will not cover all the high likelihood positions, and we might miss the correct position.

There are two man approaches to initializing the particles
* sample uniformly across the state space by dividing the state space into a grid, and put one particle in each cell. This approach is not practical if the state space is too large such as the case of self driving car where the state space spans essentially the entire land surface of the earth
* sample around some kind of initial estimate. For self driving car, this initial estimate can come from GPS. We cannot rely solely on GPS for localization because of its low accuracy and reduced availability in certain environments. However, GPS can be very useful to provide initial rough estimate of the position.

In this project, we sample from gaussian distribution, taking into account gaussian sensor noise around the initial GPS position estimate and initial heading estimate.

####  Prediction 

During the prediction step, for each particle, we will update its location based on control inputs velocity, $v$ and yaw rate, $\dot\theta$ using the bicycle motion model (Figure 4). To account for the uncertainty in the control input, we will also add gaussian noise to the $v$ and $\dot\theta$.

[image4]: ./figures/Bicycle_motion_model.jpg "Bicycle motion model equations"
![alt text][image4]
**Figure 4 Bicycle motion model equations**

####  Data association 

Before we can use landmark measurements of the objects around the car to update the belief of its position, we will have to solve the data association problem of matching landmark measurements to objects in the real world, like map landmarks. For localization, when sensor measurements (what we actually see) matches with map landmarks (what we expect to see), it must be true location of the car. Often, some landmarks have multiple sensor measurements that correspond to it. To pick the right correspondence, we use nearest neighbor (NN) approach of closest measurement as the correct correspondence. 



####  Update

The feature measurements (observations) from sensors will be used to compute the update step of the particle filter. However, instead of the feature measurements directly affecting the prediction of the state of the car, they will inform the weight of each particle. One way to update the weight of the particles is to use multivariate gaussian pdf for each measurement and combine the likelihoods by taking their product as shown in Figure 5.

[image5]: ./figures/Weight_update_equation.jpg "Weight update equation"
![alt text][image5]
**Figure 5 Weight update equations**

The weight update equation tells us how likely a set of independent landmark measurements is, given our predicted state of the car and the assumption that the sensors have gaussian noise. 

$x_{i}$ represents the sensor measurement for the $i$th map landmark, $\mu_{i}$ represents the predicted measurement of the $i$th map landmark for a single particle, $m$ is the total number of measurements for one paricle. $\sum$ is the covariance matrix of the measurements that contains the uncertainty of each variable in the sensor measurement (variance, diagonal elements), as well as the covariance between the variables (off diagonal elements). One can think of $\sum$ matrix as an inverse matrix of weights. The smaller the diagonal term for a certain variable, the more we can trust this variable in the measurements, and higher the weight we can put on it. Since we assume different variables to be independent, the off diagonal terms are zero.

For lidar, the variables in question would be $x$ and $y$ position of the landmarks in vehicle coordinates, where $x$-axis points in the direction of the car's heading, and $y$-axis points to the left of the car. 

####  Calculating error 

We assess the accuracy of our particle filter by comparing the pose estimates with the ground truth pose for every time step. Figure 6 presents two methods based on 1) weighted error, and 2) best (highest weighted) particle. 

[image6]: ./figures/Error_calculation.jpg "Error calculations"
![alt text][image6]
**Figure 6 Error calculations**

$p_{i}$ is the state vector $p$ = ($x$,$y$,$\theta$) for $i$th particle, and $g$ is ground truth state vector. $\sqrt{|p_{i}-g|}$ is a combination of position RMSE and theta RMSE. 

Position RMSE = $\sqrt{(x_{p} - x_{g})^2 + (y_{p} - y_{g})^2}$

Theta RMSE = $\sqrt{(\theta_{p} - \theta_{g})^2}$

Please note that for the kidnapped vehicle project, the ground truth data were not provided. So, we only calculate the average weight of the set of particles, and find the pose of the particle with the highest weight.

####  Transformations and associations 

To calculate the weight of each particle, which represents how well that particle fits to being in the same location as the actual car, as illustrated in Figure 7, we first need to transform the car's observation measurements from its local coordinate system to the map's coordinate system, followed by associating each transformed measurement with the closest landmark. Finally, we will use this information to update the weight of the particle, for which we have the position and the heading parameters. The weight of each particle is updated by applying multivariate gaussian pdf for each measurement, and then combining the probabilities of all the measurements by taking their products. The multivariate gaussian pdf has two dimensions, $x$ and $y$; $\mu$ is the measurement's associated landmark position; $\sigma_{x}$ and $\sigma_{y}$ is described by our initial uncertainty in the $x$ and $y$ ranges.

[image7]: ./figures/Transformation_steps.jpg "Steps in transformation and weight update"
![alt text][image7]
**Figure 7 Steps in transformation and weight update**

[image8]: ./figures/CarToMapCoord_transformation.jpg "Car to map coordinate transformation"
![alt text][image8]
**Figure 8 Car to map coordinate transformation**

In the graph depicted in Figure 8 above, we have a car (ground truth position) that observes three nearby landmarks, each one labeled OBS1, OBS2, OBS3. Each observation measurement has $x$ and $y$ values in the car's coordinate system. We have a particle "P" (estimated position of the car) above with position (4,5) on the map with heading -90 degrees. The first task is to transform each observation marker from the vehicle's coordinates to the map's coordinates, with respect to our particle.

As listed below, the transformation of observation from vehicle coordinates to map coordinates is called homogenous transformation, and is composed of a rotation and a translation.

$x_{m}$ = $x_{p}$ + cos($\theta$).$x_{c}$ - sin($\theta$).$y_{c}$

$y_{m}$ = $y_{p}$ + sin($\theta$).$x_{c}$ + cos($\theta$).$y_{c}$

$x_{c}$ and $y_{c}$ are coordinates of observation in vehicle coordinate space, $x_{p}$ and $y_{p}$ are particle coordinates, and $x_{m}$ and $y_{m}$ are transformed coordinates of observation in map coordinate space. $\theta$ is the heading direction of 
the particle.

## ** File structure ** ##

The generalized overall flow consists of initializing particle filter variables, predicting where the kidnapped vehicle is after a time step *Δt*, and updating our estimate of where our vehicle is based on sensor measurements. Then the prediction and update steps repeat themselves in a loop. To measure how well our particle filter localizer performs, we will then calculate highest weight of the best particle and the average weight of the set of particles. These three steps (initialize, predict, update) plus finding the best particle encapsulates the entire particle filter project. 

Following are the project files in the *src* folder:

* *main.cpp* - 
  * initializes the noise parameters for GPS coordinates and landmark coordinates
  * reads in map data
  * creates an instance of particle filter
  * if the particle filter is not initialized, it is initialized with the noisy sensor data from the simulator for the first time
  * if the particle filter is initialized, it receives noiseless control data from previous time step and passes that to    particle filter along with $\Delta$t (time interval between measurements) and GPS measurement uncertainties to predict the   vehicles next state 
  * receives noisy observation data of landmarks from the simulator and passes that to particle filter along with information  regarding sensor range, landmark measurement uncertainties, and map to update the weights
  * calls the particle filter to resample based on updated weights
  * calculates and outputs the average weighted error of the particle filter over all time steps so far
  * finds the best particle and passes on the pose of the best particle to the simulator.

* *particle_filter.cpp* -
  * init() 
      - sets the number of particles
      - initializes all particles to first position(based on estimates x, y, $\theta$ and their uncertainties from GPS) and              all weights to 1 
      - adds random gaussian noise to each particle.
  * prediction() 
      - performs prediction using the bicycle motion model equations 
      - prediction is done for the case of both zero and nonzero yaw rate
      - normally distributed gaussian noise is also added to the predicted values.
  * dataAssociation() - find the map landmark that is closest to each transformed observation within the range of sensor
  * updateWeights() - updates the weight of each particle in the following steps
     - Using the particle coordinates in the map coordinates, transform each observation from vehicle coordinates to map coordinates by applying homogenous transform that applies rotation with the orientation $\theta$ of each particle and translation with *x* and *y* coordinates of each particle
    - For each transformed observation, find the nearest associated landmark using dataAssociation()
    - Calculate multivariate gaussian probability using the *x* and *y* coordinates of each transformed observation and nearest landmark pair
    - Multiply the individual probabilities to calculate Bayes posterior probability, which is the updated weight of that particle
    - Repeat the above steps for all the particles
    - Finally, normalize the weights of particles to [0,1] range, which will be useful in the resampling step.                   
  * resample() - Resample the particles using std::discrete_distribution. The method resamples with replacement according to the weights of the particles, preference being to given to the particles with higher weights.        

The Term 2 simulator is a client, and the C++ program software is a web server. *main.cpp* reads in the sensor data line by line from the client and passes to the particle filter for processing. *main.cpp* is made up of several functions within main(), these all handle the uWebsocketIO communication between the simulator and it's self.

### ** Data ** ###

The simulator provides noisy position data, vehicle controls, and noisy observations. The script (main.cpp) feeds back the state ($x$, $y$, $\theta$) of the best particle to the simulator at each time step.


## Results 

Experiments were carried out for the following:
1. for different values of GPS measurement uncertainty
2. for different values of landmark measurement uncertainty
3. minimum number of particles required for convergence for each pair of 1 and 2. 

The error of the best particle ($P_{x}$, $P_{y}$, $P_{\theta}$), number of particles, and time to success are recorded for each experiment and tabulated below.

|$GPS_{x}$|$GPS_{y}$|$GPS_{\theta}$|$L_{x}$|$L_{y}$|$P_{x}$|$P_{y}$|$P_{\theta}$|$N_{p}$|$T_{c}$(s)|     
|:-------:|:-------:|:------------:|:-----:|:-----:|:-----:|:-----:|:----------:|:-----:|:--------:|     
|  0.1    |  0.1    |  0.005       |  0.1  | 0.1   |  0.08 |  0.08 | 0.003      |  45   | 95.74    |
|  3.0    |  3.0    |  0.1         |  3.0  | 3.0   |  0.8  |  0.8  | 0.03       |  45   | 92.80    |
|  0.1    |  0.1    |  0.005       |  0.1  | 0.1   |  0.08 |  0.08 | 0.003      |  25   | 68.90    |
|  3.0    |  3.0    |  0.1         |  3.0  | 3.0   |  0.8  |  0.8  | 0.03       |  25   | 67.68    |
|  0.1    |  0.1    |  0.005       |  0.1  | 0.1   |  0.2  |  0.1  | 0.004      |  11   | 50.80    |
|  3.0    |  3.0    |  0.1         |  3.0  | 3.0   | N/A   |  N/A  | N/A        |  11   | N/A      |

As shown in Figure 9, the stipulated time to success requirement of converging within 100s was achieved with as few as 11 particles under low GPS measurement uncertainties and low landmark measurement uncertainties.

[image9]: ./figures/Proof_of_success.jpg "Kidnapped vehicle localized"
![alt text][image9]
**Figure 9 Kidnapped vehicle localized**


```python
# <video controls src="videos/ParticleFilter_demo.mp4" />
```

### **Acknowledgments**

I would like to thank Udacity for giving me this opportunity to work on an awesome project. Special thanks to the author of the blog post that helped me setup the Visual studio environment for this project - http://www.codza.com/blog/udacity-uws-in-visualstudio.

