/*
 *  particle_fliter.cpp
 *  Created on: Dec 12, 2016 Author: Tiffany Huang
 *  Modified on: August, 2018 Author: Darshan Iyer
 */

#include <random>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <math.h> 
#include <iostream>
#include <sstream>
#include <string>
#include <iterator>

#include "particle_filter.h"

using namespace std;

const int num_par = 11; // define number of particles

void ParticleFilter::init(double x, double y, double theta, double std[]) 
{
	if (!initialized())
	{
		/*Set the number of particles.Initialize all particles to first position(based on estimates of
		  x, y, theta and their uncertainties from GPS) and all weights to 1. 
		  Add random Gaussian noise to each particle.
		  NOTE: Consult particle_filter.h for more information about this method (and others in this file).*/

		default_random_engine gen;
		// This line creates a normal (Gaussian) distribution.
		normal_distribution<double> dist_x(x, std[0]); // create normal distribution with mean x and GPS meas uncertainty in x-co-ordinate
		normal_distribution<double> dist_y(y, std[1]); // create normal distribution with mean y and GPS meas uncertainty in y-co-ordinate
		normal_distribution<double> dist_theta(theta, std[2]); // create normal distribution with mean theta and GPS meas uncertainty in orientation theta

		num_particles = num_par; // initialize number of particles
		particles.resize(num_par); // resize particles vector to be able to hold number of particles
		weights.resize(num_par); // resize weights vector to be able to hold number of particles

		for (int i = 0; i < num_particles; ++i)
		{
			particles[i].id = i;
			particles[i].x = dist_x(gen); // initialize with a sample generated randomly from above created normal distribution
			particles[i].y = dist_y(gen);
			particles[i].theta = dist_theta(gen);
			particles[i].weight = 1.0;  // initialize weights to 1
			weights[i] = 1.0;
		}

		is_initialized = true; // set is_initialized to true so that initialization is not done again
	}	

	return;
}

/* 
Perform prediction using the bicycle motion model equations. 
Prediction is done for the case of both zero and nonzero yawrate.
Normally distributed gaussian noise is also added to the predicted values.
*/
void ParticleFilter::prediction(double delta_t, double std_pos[], double velocity, double yaw_rate)
{
	// TODO: Add measurements to each particle and add random Gaussian noise.
	// NOTE: When adding noise you may find std::normal_distribution and std::default_random_engine useful.
	//  http://en.cppreference.com/w/cpp/numeric/random/normal_distribution
	//  http://www.cplusplus.com/reference/random/default_random_engine/
	
	default_random_engine gen;

	double x = 0;
	double y = 0;
	double theta = 0;

	for (int i = 0; i < num_particles; ++i)
	{
		double yaw_angle = particles[i].theta;

		if (fabs(yaw_rate) > 0.001) // nonzero yawrate 
		{
			x = particles[i].x + (velocity / yaw_rate)*(sin(yaw_angle + (yaw_rate*delta_t)) - sin(yaw_angle));
			y = particles[i].y + (velocity / yaw_rate)*(-cos(yaw_angle + (yaw_rate*delta_t)) + cos(yaw_angle));
		}
		else // zero yawrate
		{
			x = particles[i].x + (velocity*cos(yaw_angle)*delta_t);
			y = particles[i].y + (velocity*sin(yaw_angle)*delta_t);
		}
		theta = particles[i].theta + (yaw_rate*delta_t);

		normal_distribution<double> dist_x(x, std_pos[0]);
		normal_distribution<double> dist_y(y, std_pos[1]);
		normal_distribution<double> dist_theta(theta, std_pos[2]);

		particles[i].x = dist_x(gen);
		particles[i].y = dist_y(gen);
		particles[i].theta = dist_theta(gen);
	}

	return;
}

/*
Find the map landmark that is closest to each transformed observation
Collect the x and y co-ordinate of both in assocPair struct 
Return a vector of assocPair struct that contains all the associated pairs.
*/
std::vector<assocPair> ParticleFilter::dataAssociation(double sensor_range, std::vector<LandmarkObs> transformed_obs,
	                                                   std::vector<LandmarkObs>& map_landmarks)
{
	std::vector<assocPair> assocPair_out(transformed_obs.size());
	for (int i = 0; i < transformed_obs.size(); ++i)
	{
		double min_dist = sensor_range;
		double temp = 0.0;
		int sel_id = -1;
		int min_index = -1;
		for (int j = 0; j < map_landmarks.size(); ++j)
		{
			temp = dist(transformed_obs[i].x, transformed_obs[i].y, map_landmarks[j].x, map_landmarks[j].y);
			if (temp < min_dist)
			{
				min_dist = temp;
				sel_id = map_landmarks[j].id;
				min_index = j;
			}
		}
		transformed_obs[i].id = sel_id;
		assocPair_out[i].x_1 = transformed_obs[i].x;
		assocPair_out[i].y_1 = transformed_obs[i].y;
		assocPair_out[i].x_2 = map_landmarks[min_index].x;
		assocPair_out[i].y_2 = map_landmarks[min_index].y;
	}

	return assocPair_out;
}

/*
Update the weight of each particle in the following steps
1) Using the particle co-ordinates in the map co-ordinates, 
   transform each observation from vehicle co-ordinates to
   map co-ordinates by applying homogenous transform that 
   applies rotation with the orientation theta of each particle
   and translation with x and y co-ordinates of each particle
2) For each transformed observation, find the nearest associated
   landmark
3) Calculate multivariate gaussian probability using the x and y 
   co-ordinates of each transformed observation and nearest landmark pair
4) Multiply the individual probabilities to calculate Bayes 
   posterior probability, which is the updated weight of that particle
5) Repeat the above steps for all the particles
6) Finally, normalize the weights of particles to [0,1] range, which
   will be useful in the resampling step.
*/
void ParticleFilter::updateWeights(double sensor_range, double std_landmark[], 
								   const std::vector<LandmarkObs>& observations, 
								   const Map& map_landmarks) 
{
	// TODO: Update the weights of each particle using a mult-variate Gaussian distribution. You can read
	//   more about this distribution here: https://en.wikipedia.org/wiki/Multivariate_normal_distribution
	// NOTE: The observations are given in the VEHICLE'S coordinate system. Your particles are located
	//   according to the MAP'S coordinate system. You will need to transform between the two systems.
	//   Keep in mind that this transformation requires both rotation AND translation (but no scaling).
	//   The following is a good resource for the theory:
	//   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
	//   and the following is a good resource for the actual equation to implement (look at equation 
	//   3.33
	//   http://planning.cs.uiuc.edu/node99.html

	// Convert landmark_list contained in map_landmarks struct to vector of landmarks.
	// This conversion is done to make the input compatible with dataAssociation()
	std::vector<LandmarkObs> landmarks(map_landmarks.landmark_list.size());
	for (int k = 0; k < landmarks.size(); ++k)
	{
		landmarks[k].x = map_landmarks.landmark_list[k].x_f;
		landmarks[k].y = map_landmarks.landmark_list[k].y_f;
		landmarks[k].id = map_landmarks.landmark_list[k].id_i;
	}

	std::vector<LandmarkObs> transformed_obs(observations.size()); // Define a vector that store observations in map co-ordinates

	/*
	Using the particle co-ordinates in the map co-ordinates,
	transform each observation from vehicle co-ordinates to
	map co-ordinates by applying homogenous transform that
	applies rotation with the orientation theta of each particle
	and translation with x and y co-ordinates of each particle
	*/
	for (int i = 0; i < num_particles; ++i)
	{
		for (int j = 0; j < observations.size(); ++j)
		{
			transformed_obs[j].id = observations[j].id;
			// transform to map x coordinate
			transformed_obs[j].x = particles[i].x + (cos(particles[i].theta) * observations[j].x) - (sin(particles[i].theta) * observations[j].y);

			// transform to map y coordinate
			transformed_obs[j].y = particles[i].y + (sin(particles[i].theta) * observations[j].x) + (cos(particles[i].theta) * observations[j].y);
		}

		/*
		For each transformed observation, find the nearest associated landmark.
		Repeat this for all the observations.
        */
		std::vector<assocPair> assocPair_out = dataAssociation(sensor_range, transformed_obs, landmarks);

		double weight = 1;
		for (int l = 0; l < assocPair_out.size(); ++l)
		{
			/*
			Calculate multivariate gaussian probability using the x and y
			co-ordinates of each transformed observation and nearest landmark pair
			*/
			/*
			Multiplying the individual probabilities to calculate total probability, 
			which is the updated weight of that particle
			*/
			weight *= multiVarGaussianProb(std_landmark[0], std_landmark[1], assocPair_out[l].x_1, assocPair_out[l].y_1,
				assocPair_out[l].x_2, assocPair_out[l].y_2);
		}
		particles[i].weight = weight;
		weights[i] = weight;
	}

	// Normalize the weights to values between 0 and 1
	weights = normalize_vector(weights);

	return;
}

/*
Resample the particles using std::discrete_distribution
The method resamples with replacement according to the weights of the particles,
preference being to given to the particles with higher weights
*/
void ParticleFilter::resample() 
{
	// TODO: Resample particles with replacement with probability proportional to their weight. 
	// NOTE: You may find std::discrete_distribution helpful here.
	//   http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution

	std::default_random_engine generator;
	std::discrete_distribution<int> distribution(weights.begin(), weights.end());

	std::vector<Particle> temp_particles(num_particles);
	for (int i = 0; i < num_particles; ++i)
	{
		int number = distribution(generator);
		temp_particles[i] = particles[number];
	}
	particles = temp_particles;

	return;
}

/*
Calculate probability of bi-variate gaussian distribution
*/
double ParticleFilter::multiVarGaussianProb(double sig_x, double sig_y, double x_obs, double y_obs, double mu_x, double mu_y)
{
	// calculate normalization term
	double gauss_norm = (1 / (2*M_PI*sig_x*sig_y));

	// calculate exponent
	double exponent = ((x_obs - mu_x)*(x_obs - mu_x)) / (2*sig_x*sig_x) + ((y_obs - mu_y)*(y_obs - mu_y)) / (2*sig_y*sig_y);

	// calculate weight using normalization terms and exponent
	double prob = gauss_norm*exp(-exponent);

	return prob;
}

/*
Normalize the input vector so that the values are between 0 and 1
*/
std::vector<double> ParticleFilter::normalize_vector(std::vector<double> inputVector)
{
	//declare sum:
	double sum = 0.0f;

	//declare and resize output vector:
	std::vector<double> outputVector;
	outputVector.resize(inputVector.size());

	//estimate the sum:
	for (unsigned int i = 0; i < inputVector.size(); ++i)
	{
		sum += inputVector[i];
	}

	//normalize with sum:
	for (unsigned int i = 0; i < inputVector.size(); ++i)
	{
		outputVector[i] = inputVector[i] / sum;
	}

	//return normalized vector:
	return outputVector;
}

Particle ParticleFilter::SetAssociations(Particle& particle, const std::vector<int>& associations,
	const std::vector<double>& sense_x, const std::vector<double>& sense_y)
{
	//particle: the particle to assign each listed association, and association's (x,y) world coordinates mapping to
	// associations: The landmark id that goes along with each listed association
	// sense_x: the associations x mapping already converted to world coordinates
	// sense_y: the associations y mapping already converted to world coordinates

	//Clear the previous associations
	particle.associations.clear();
	particle.sense_x.clear();
	particle.sense_y.clear();

	particle.associations = associations;
	particle.sense_x = sense_x;
	particle.sense_y = sense_y;

	return particle;
}

string ParticleFilter::getAssociations(Particle best)
{
	vector<int> v = best.associations;
	stringstream ss;
	copy(v.begin(), v.end(), ostream_iterator<int>(ss, " "));
	string s = ss.str();
	s = s.substr(0, s.length() - 1);  // get rid of the trailing space
	return s;
}
string ParticleFilter::getSenseX(Particle best)
{
	vector<double> v = best.sense_x;
	stringstream ss;
	copy(v.begin(), v.end(), ostream_iterator<float>(ss, " "));
	string s = ss.str();
	s = s.substr(0, s.length() - 1);  // get rid of the trailing space
	return s;
}
string ParticleFilter::getSenseY(Particle best)
{
	vector<double> v = best.sense_y;
	stringstream ss;
	copy(v.begin(), v.end(), ostream_iterator<float>(ss, " "));
	string s = ss.str();
	s = s.substr(0, s.length() - 1);  // get rid of the trailing space
	return s;
}
